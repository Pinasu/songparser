import sys, os, csv, operator
from itertools import chain

#	To print the first argument
#	argv[0] is 'main.py'
#
#		print(sys.argv[1])


#	dict_from_file (file, dictionary)
#	For every line in file adds words or counters to dict dictionary
#	
#	params: 	file pointer, dict object
#
def dict_from_file (file, dictionary, exclude_list):
	for line in open(file, "r"):
		line = clear_line(line)

		if line != "":
			line_lst = get_clean_lst(line)
		
			for word in line_lst:
		
				if word != "":
				#	The word is to be ignored
					if word not in exclude_list:
						if word in dictionary:
							dictionary[word] += 1
						else:
							dictionary[word] = 1


#	iterate_files (path)
#	For every file in path calls dict_from_file
#	
#	params: 	file path
#	returns:	a processed dict
#
def iterate_files (main_path, exclude_file):
	dct = dict()
	exclude = get_words_to_exclude(exclude_file)
	for file in os.listdir(main_path):
		dict_from_file(main_path + file, dct, exclude)

	return dct


#	clear_line (line)
#	Removes useless characters from string
#	
#	params: 	a string
#	returns:	a processed string
#
def clear_line (line):
	return line.replace("\'", "").replace("\n", "").replace(".", "").replace("-", "")


#	get_clean_lst (str)
#	Removes stopwords from string
#	
#	params: 	a string
#	returns:	a processed list
#
def get_clean_lst (str):
	lst = str.split(" ")
	return lst


#	get_excluded_words (file_path)
#	Get a list of words not to be considered from csv file
#	
#	params: 	a file path
#	returns:	a processed list
#
def get_words_to_exclude (file_path):
	with open(file_path, 'r') as file:
		reader = csv.reader(file)
		lst = list(reader)
		mylist = map(list, zip(*lst[:len(lst)]))
		return list(chain.from_iterable(mylist))

def main():
	if not sys.argv[1]:
		print('Error. You need to specify /home/pinasu/Documenti/Programs/De Andre Parser/in/files path as script parameter.')
	else:
		sorted_map = sorted((iterate_files(sys.argv[1], sys.argv[2])).items(), key=operator.itemgetter(1), reverse=True)
		
		#	Only takes first 20
		print(sorted_map[:20])

main()